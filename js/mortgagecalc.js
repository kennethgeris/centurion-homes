pd = 'paydata'; /* SAVING CALCULATOR DATA */
numDays = 60;  /* DAYS UNTIL COOKIE EXPIRES (EG. 183 DAYS = 6 MONTHS) */

/* FUNCTION THAT ADDS COMMAS TO THE CURRENCY VALUE (i.e. $1000000.00 -> $1,000,000.00) */

function addCommas(currencyValue) {
    currencyValue = "" + currencyValue;
    var dollars = 0;
    var cents = "";
    if (currencyValue.indexOf(",") != -1) {
        return currencyValue;
    } else if (currencyValue.indexOf(".") != -1) {
        dollars = currencyValue.substring(0, currencyValue.indexOf("."));
        cents = currencyValue.substring(currencyValue.indexOf("."), currencyValue.length);
    } else if (currencyValue > 0) {
        dollars = currencyValue;
    } else {
        return currencyValue;
    }
    var returnValue = "";
    for (var i = 1; i <= dollars.length; i++) {
        if (i % 3 == 1 && i != 1) {
            returnValue = "," + returnValue;
        }
        returnValue = dollars.charAt(dollars.length - i) + returnValue;
    }
    returnValue += cents;
    return returnValue;
}


/*  THE FOLLOWING TWO FUNCTIONS CHECK THAT ALL NUMERIC VALUES ARE REAL NUMBER AND REMOVE DOUBLE DECIMALS */

function check(a)
{
    var pest = 0;
    var b = "";
    for (i = 0; i <= a.length; i++)
    {
        var u = a.charAt(i);
        if ((u >= "0" && u <= "9") || u == ".")
        {
            if (u == ".") {
                var pest = pest + 1;
                if (pest == 2) {
                    break;
                }
            }
            var b = b + u;
        }
    }
    return b;
}

function doSum(a) {
    a.value = check(a.value);
}


/* TESTS VERSIONS FOR WHICH WILL SUPPORT POP UP WINDOWS */
function versTest()
{
    var one = '';
    var two = '';

    if (
            (navigator.appName.substring(0, 8) == "Netscape" && (navigator.appVersion.substring(0, 3) == "3.0" || navigator.appVersion.substring(0, 3) == "4.0")))
    {
        var one = 'true';
    }
    if (
            (navigator.appName.substring(0, 9) == "Microsoft" && navigator.appVersion.substring(0, 3) == "3.0" && navigator.appVersion.indexOf("Macintosh") >= 0))
    {
        var two = 'true';
    }

    if (one == 'true' || two == 'true' ||
            (navigator.appName.substring(0, 9) == "Microsoft" && navigator.appVersion.indexOf("MSIE 3.0") >= 0 &&
                    navigator.appVersion.indexOf("Windows 3.1") >= 0)
            )
    {
        return true;
    } else
    {
        return false;
    }

}

/* TESTS IF VERSION IS MSIE 3.0 FOR MAC */
function msTest()
{
    if (navigator.appName.substring(0, 9) == "Microsoft" && (navigator.appVersion.substring(0, 3) == "3.0" && navigator.appVersion.indexOf("Macintosh") >= 0))
    {
        return true;
    } else
    {
        return false;
    }

}

function nineTest()
{
    if (navigator.appName.substring(0, 9) == "Microsoft" && (navigator.appVersion.substring(0, 3) == "3.0" || navigator.appVersion.indexOf("MSIE 3.0") >= 0) && (navigator.appVersion.indexOf("Macintosh") == -1 || navigator.appVersion.indexOf("Windows 3.1") == -1)
            )
    {
        return true;
    } else
    {
        return false;
    }
}

/* OPENS POP UP WINDOW TO DISPLAY VALIDATION MESSAGES IN NETSCAPE 3.0 AND 4.0 */

function fixpro(n, q)

{
    if (versTest() == true) {
        if (msTest() == true) {
            var winNam = '';
        } else {
            var slash = location.href.lastIndexOf("/") + 1;
            var filNam = location.href.substring(0, slash);
            var winNam = filNam + 'empty.html';
        }
        fix = window.open(winNam, 'FIX', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=100');
        if (navigator.appName.substring(0, 8) == "Netscape") {
            fix.focus();
        }
        fix.document.write('<html><head><title>Calculators</title>');
        fix.document.write('</head><body bgcolor=ffffff><form name=fixer>');
        fix.document.write('<font size=2 face="Arial, Helvetica" color=306798>' + n + '</font><p><font size=2 face="Arial, Helvetica">' + q + '<p>');
        fix.document.write('<center><input type=button value=OK onClick=self.close()>');
        fix.document.write('</center></form></body></html>');
        fix.document.close();
    } else {
        alert(n + '\n' + q)
    }
}

/* FUNCTION CONFIRMS THAT THE VALUE ENTERED INTO A FIELD FALLS WITHIN THE PRE-DETERMINED MINIMUM AND MAXIMUM VALUES, AND DISPLAYS AN ERROR MESSAGE WITH THE ALLOWABLE NUMERIC RANGE FOR THE FIELD DATA IN A POP UP VALIDATION WINDOW */
function checkNumber(quest, input, min, max, msg)
{
    var str = input.value;
    for (var i = 0; i < str.length; i++) {
        var ch = str.substring(i, i + 1)
        if ((ch < "0" || "9" < ch) && ch != '.') {
            alert(msg);
            return false;
        }
    }
    if (input.value != "")
    {
        var num = 0 + str
        if (num < min || max < num) {
            var sendn = "Question " + quest + ": (" + msg + ")";
            var sendq = "You have entered " + input.value + ". Please enter a number between " + min + " and " + max + ".";
            fixpro(sendn, sendq);

            return false;
        }
        input.value = str;
        return true;
    }
}

/* CALLS UPON THE FUNCTIONS TO DETERMINE IF THE NUMBERS ENTERED ARE VALID AND TO CALCULATE THE RESULTS OF THE ENTERED DATA FOR EXAMPLE - MORTGAGE PAYMENT, GDS AND TDS RATIOS, AND LOAN TO VALUE. THIS FUNCTION IS EXECUTED EVERYTIME A VALUE IS CHANGED IN A FIELD */
function computeField(quest, input, min, max, msage)
{
    doSum(input);
    return (checkNumber(quest, input, min, max, msage));
}

/* RETURNS THE SELECTED INDEX VALUE OF SELECT LISTS IN THE CALCULATOR TO BE USED IN CALCULATIONS */
function getIndex(n) {
    return n.selectedIndex;
}

function calcRdefine(intrate, compound, freq) {
    return Math.pow((1.0 + ((intrate / 100) / compound)), (compound / freq)) - 1.0;
}

function calcBal(mortgage, intrate, compound, freq, payment, term) {
    rdefine = calcRdefine(intrate, compound, freq);
    return (mortgage * (Math.pow((1.0 + rdefine), (term)))) - ((payment * ((Math.pow((1.0 + rdefine), (term))) - 1.0)) / rdefine);
}

/* ROUNDS OFF MONETARY NUMBERS TO TWO DECIMALS (PENNIES) */

function roundPen(n)
{
    if (n > 0) {
        pennies = n * 100;
        pennies = Math.round(pennies);
        strPennies = "" + pennies;
        len = strPennies.length;
        return strPennies.substring(0, len - 2) + "." + strPennies.substring(len - 2, len);
    } else
        return 0;
}

/* THIS FUNCTION CALCULATES THE LOAN TO VALUE RATIO */
function LTVcalc(MORTGAGE, MORTGAGE2, APPRAISE) {
    return (MORTGAGE / APPRAISE) + (MORTGAGE2 / APPRAISE);
}


function Ratios(PAY1, PAY2, HEAT, TAX, DEBT, INCOME) {
    return (PAY1 / INCOME) + (PAY2 / INCOME) + (HEAT / INCOME) + (TAX / INCOME) + (DEBT / INCOME);
}

/* THIS FUNCTION CALCULATES THE MONTHLY MORTGAGE PAYMENT BASED ON THE USER'S INPUT */
function calcPay(MORTGAGE, AMORTYEARS, INRATE, COMPOUND, FREQ) {
    var compound = COMPOUND / 12;
    var monTime = (AMORTYEARS * 12);
    var RATE = (INRATE * 1.0) / 100;
    var yrRate = RATE / COMPOUND;
    var rdefine = Math.pow((1.0 + yrRate), compound) - 1.0;
    var PAYMENT = (MORTGAGE * rdefine * (Math.pow((1.0 + rdefine), monTime))) / ((Math.pow((1.0 + rdefine), monTime)) - 1.0);
    if (FREQ == 12) {
        return PAYMENT;
    }
    if (FREQ == 26) {
        return PAYMENT / 2.0;
    }
    if (FREQ == 52) {
        return PAYMENT / 4.0;
    }
    if (FREQ == 24) {
        var compound2 = COMPOUND / FREQ;
        var monTime2 = (AMORTYEARS * FREQ);
        var rdefine2 = Math.pow((1.0 + yrRate), compound2) - 1.0;
        var PAYMENT2 = (MORTGAGE * rdefine2 * (Math.pow((1.0 + rdefine2), monTime2))) / ((Math.pow((1.0 + rdefine2), monTime2)) - 1.0);
        return PAYMENT2;
    }
}

function retTerm(n) {
    if (n == 0) {
        return 0;
    }
    if (n == 1) {
        return 6;
    }
    if (n == 2) {
        return 12;
    }
    if (n == 3) {
        return 24;
    }
    if (n == 4) {
        return 36;
    }
    if (n == 5) {
        return 60;
    }
    if (n == 6) {
        return 84;
    }
    if (n == 7) {
        return 120;
    }
}

function retFreq(n) {
    if (n == 0) {
        return 0;
    }
    if (n == 1) {
        return 12;
    }
    if (n == 2) {
        return 24;
    }
    if (n == 3) {
        return 26;
    }
    if (n == 4) {
        return 52;
    }
}
function calcTotal(MORTGAGE, LTV) {
    if (LTV > .75 && LTV <= .80) {
        return MORTGAGE * 1.0125;
    }
    if (LTV > .80 && LTV <= .85) {
        return MORTGAGE * 1.02;
    }
    if (LTV > .80) {
        return MORTGAGE * 1.025;
    }
    if (LTV <= .75) {
        return MORTGAGE
    }
}

/* SAVES COOKIE CONTAINING DATA TO BE USED IN AMORTIZATION SCHEDULE */
function quickCook(MTGAMT, AMORTYEARS, RATE, TERM, FREQ, WHATYEAR)
{
    if (WHATYEAR && WHATYEAR == -1) {
        var timeKindA = 0;
    } else {
        WHATYEAR = 0;
        var timeKindA = 1;
    }


    var AMORTPMT = calcPay(MTGAMT, AMORTYEARS, RATE, 2, FREQ);
    var intrate = RATE / 100;
    var INTFACTOR = Math.pow((1 + intrate / 2), (2 / FREQ)) - 1;
    var expire = new Date();
    expire.setTime(expire.getTime() + (numDays * 24 * 3600000));/* 2 MONTHS */
    var pdData = " ";

    pdData = pdData + '`' + MTGAMT + '`' + AMORTPMT + '`' + INTFACTOR + '`' + FREQ + '`' + ((AMORTYEARS * FREQ)) + '`' + 0 + '`' + 0 + '`' + (TERM / 12) + '`' + timeKindA + '`' + RATE * 1.0 / 100 + '`' + 0 + '`' + WHATYEAR + '`' + 0 + '`' + 0 + '`' + 0 + '`' + 0 + '`' + 0;
    document.cookie = pd + "=" + escape(pdData) + "; expires=" + expire.toGMTString() + "; path=/";
}

/* VALIDATES ALL THE FIELDS AND CALCULATES VALUES TO BE ENTERED INTO THE TEXT BOXES AT THE BOTTOM OF THE PAGE WHEN THE USER CLICKS ON COMPUTE OR COMPUTE AMORTIZATION */

function compute(form, year) {
    term = retTerm(document.paycalc.desterm.selectedIndex);
    freq = retFreq(document.paycalc.PFREQ.selectedIndex);
    amortyears = document.paycalc.NAMORTYEARS.value;
    mortgage = document.paycalc.mortamt.value;
    intrate = document.paycalc.rate.value;
    var payment = calcPay(mortgage, amortyears, intrate, 2, freq);
    var intFact = Math.pow((1 + (intrate / 100) / 2), (2 / freq)) - 1;


// need to determin the number of payments so we can correctly determin the Amortization period
// and the remaing balance
    var principalBal = mortgage;
    var aYear = 0;
    var annualCount = 1;


    for (payAdd = 1; principalBal > 0; payAdd++) {

        var checkLump = 0;

        var printPayments = Math.ceil(1 / freq);//Expressed in Years    
        var printYear = printPayments * freq;//Expressed in Payments



        //SET FLAG IF THIS PAYMENT IS AN ANNIVERSARY
        if ((payAdd - 1) / Math.floor(freq) == annualCount) {
            var aYear = 1;
        }

        //INTEREST AND PRINCIPAL PAYMENT
        var interestPayment = roundPen(principalBal * intFact) * 1.0;
        var principalPayment = roundPen(payment - interestPayment) * 1.0;
        var checkBal = (principalPayment > principalBal) ? principalBal : principalPayment;
        var principalBal = roundPen(principalBal - checkBal) * 1.0;

        //LUMPSUM PAYMENT IF REQUESTED
        if (aYear == 1 && 0 >= annualCount) {
            var checkLump = (lumpAmt > principalBal) ? principalBal : lumpAmt;
            var principalBal = roundPen(principalBal - checkLump) * 1.0;
        }

        //ELIMINATES BALANCE (ADDS IT TO PRINCIPAL PAID) IF LESS THAN HALF
        if (principalBal < ((interestPayment + principalPayment) / 2)) {
            var checkBal = checkBal + principalBal;
            var principalBal = 0;
        }

        //RESETS THE ANNIVERSARY
        if (aYear == 1) {
            var annualCount = annualCount + 1;
            var aYear = 0;
        }

    }


    var totalYears = (payAdd - 1) / freq;

    var oForm = document.forms["paycalc"];
    oForm.mainpay.value = '$' + addCommas(roundPen(payment));

    quickCook(mortgage, amortyears, intrate, term, freq, year);
    return true;
}

/* OPENS WINDOW USED TO DISPLAY HELP MESSAGES WHEN THE USER CLICKS ON A HELP BUTTON. THE HELP MESSAGE DISPLAYED IS DETERMINED IN THE ARRAY WHICH IS REFERENCED ACCORDING TO THE HELP BUTTON WHICH WAS CLICKED */
function winopen(name)
{
    var linkit = "help/" + name;
    if (versTest() == true || nineTest() == true) {
        qc = window.open(linkit, 'helpscreen', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=250,height=180');
        if (navigator.appName.substring(0, 8) == "Netscape")
        {
            qc.focus();
        }

    } else {
        location.href = linkit;
    }
}

function StringArray(n)
{
    this.length = n;
    for (var i = 1; i <= n; i++)
        this[i] = ''
    return this
}

function payBal()
{
    if (navigator.appVersion.substring(0, 3) == 2.0 && navigator.appName.substring(0, 8) == "Netscape" && navigator.appVersion.indexOf("Macintosh") >= 0) {
        setTimeout("compute(document.forms[0])", 200);
    } else {
        compute(document.forms[0]);
    }
}
