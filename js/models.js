/*setTimeout(function () {
 var $grid = $('#models').isotope({
 itemSelector: '.model',
 getSortData: {
 collection: '[data-collection]',
 sf: '[data-sf]'
 }
 });
 var filterFns = {};
 $('.filter-select').on('change', function () {
 var filterValue = this.value;
 filterValue = filterFns[ filterValue ] || filterValue;
 $grid.isotope({filter: filterValue});
 });
 
 $('.sort-select').on('change', function () {
 var sortByValue = this.value;
 $grid.isotope({sortBy: sortByValue});
 });
 }, 2000);
 */
$(document).ready(function () {

    $(".modelfb").fancybox();

    function modelOutput(type, community) {
        var jqxhr = $.getJSON("../../models.json", function (data) {
            //console.log("success");
        }).done(function (data) {
            var items = item = [];

            if (type === 'all') { // - - ALL MODELS - - //
                $.each(data, function (key, val) {
                    if (val.community === community) {
                        getName = '<div class="name">' + val.name + '</div>';
                        getImage = '<a href="?model=' + val.name + '&elev=' + val.elevation + '"><img src="../../renderings/' + val.image + '_s.jpg" /></a>';
                        getElevation = '<div class="elevation text-center" style="font-size:12px">ELEV. ' + val.elevation + ' | ' + val.sf + ' SQ. FT.</div>';
                        items.push("<div class='col-xs-4'>" + getName + getImage + getElevation + "</div>");
                    }
                });
                allModels = items.join("");
                $(allModels).appendTo("#models");

            } else { // - - SINGLE MODEL - - //

                var rendering = [];
                var elevation = [];
                var sf = [];
                $.each(data, function (key, val) {
                    if (val.name === type) {
                        elevation.push(val.elevation);
                        sf.push(val.sf);
                        rendering.push(val.image);
                        community = val.communityname;
                        storey = val.storey;
                        name = val.name;
                        price = val.price;
                        beds = val.bed;
                        bath = val.bath;
                        file = val.file;
                        floorplans = val.floorplans;
                        getName = '<div class="name">' + val.name + '</div>';
                        getImage = '<img src="../../renderings/' + val.image + '_s.jpg" />';
                        getElevation = '<div class="elevation text-center">ELEV. ' + val.elevation + ' | ' + val.sf + ' SQ. FT.</div>';
                        item.push("<div class='col-md-4'>" + getName + getImage + getElevation + "</div>");
                    }
                });

                thsElev = getUrlParameter('elev');
                otherElev = 0;
                for (var i = 0, len = item.length; i < len; i++) {
                    if (elevation[i] === thsElev) {
                        $("#community .txt").html(community);
                        $("#storey").html(storey);
                        $("#modelname").html(name);
                        $("#filelink").attr("href", "../../files/" + file);
                        $("#sf").html(sf[i]);
                        $("#mainmodel .modelimage").html('<a href="../../renderings/' + rendering[i] + '_l.jpg" class="modelfb" rel="model"><img src="../../renderings/' + rendering[i] + '_s.jpg"/></a>');
                        $("#mainmodel .details").html("ELEV. " + elevation[i] + " | " + sf[i] + " SQ. FT.");
                        fp = floorplans.split("|");

                        $("#floorplans > div").eq(0).html('<a href="../../floorplans/' + fp[0] + '" class="modelfb" rel="floorplans" title="GROUND FLOOR"><img src="../../floorplans/' + fp[0] + '"/></a>');
                        $("#floorplans > div").eq(1).html('<a href="../../floorplans/' + fp[1] + '" class="modelfb" rel="floorplans" title="SECOND FLOOR"><img src="../../floorplans/' + fp[1] + '"/></a>');
                        if (fp[2]) {
                            $("#floorplans > div").eq(2).html('<a href="../../floorplans/' + fp[2] + '" class="modelfb" rel="floorplans" title="BASEMENT"><img src="../../floorplans/' + fp[2] + '"/></a>');
                            $("#levels > div").eq(2).removeClass('hidden');
                        }
                    } else {
                        thsElement = $("#elevations .elevation").eq(otherElev);
                        $(".modelimage", thsElement).html('<a href="../../renderings/' + rendering[i] + '_l.jpg" class="modelfb" rel="model"><img src="../../renderings/' + rendering[i] + '_s.jpg"/></a>');
                        $(".details", thsElement).html("ELEV. " + elevation[i] + " | " + sf[i] + " SQ. FT.");
                        otherElev++;
                    }
                }

                $("#pricing").hide();
                $("#rooms").html(beds + " Bedrooms; " + bath + " Bathrooms");
                if (price) {
                    $("#pricing").show();
                    $("#price").html(price);
                }
                //console.log(item[0]);
            }
        }).fail(function () {
            //console.log("error");
        }).always(function () {
            if (type === 'all') {
                setColumns();
            }
        });
    }

    function setColumns() {
        setTimeout(function () {
            divH = 0;
            $("#models .col-xs-4").css("height", "auto");
            $("#models .col-xs-4").each(function () {
                //console.log($(this, "img").height());
                if ($(this).height() > divH) {
                    divH = $(this).height();
                }
            });
            $("#models .col-xs-4").height(divH);
            //console.log("complete");
        }, 200);
    }
    $(window).resize(setColumns);

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var model = getUrlParameter('model');

    if (model) {
        $(".communities, .pagepad").hide();
        $(".link5").show();
        $("#singlemodel").removeClass("hidden");
        modelOutput(model);
    } else {
        if ($("#models").length) {
            modelOutput("all", "grandriverwoods");
        }
    }
    $("#link4").click(function () {
        modelOutput("all", "grandriverwoods");
    });

    // - - - - - - - - - - HASHTAG LISTENER ON LOAD (THANK YOU)- - - - - - - - - - //


    var hash = window.location.hash;
    if (hash === "#fp") {
        setTimeout(function () {
            $("#link4 a").trigger("click");
        }, 250);
    }

    $("#link4 a").on("click", function () {
        setTimeout(function () {
            $('html, body').animate({
                scrollTop: $(".link4").offset().top
            }, 1500);
        }, 500);
    });

});