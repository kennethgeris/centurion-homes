$(document).ready(function () {
    function lttCalc() {        
        Price = $("#ltt-price").val();
        if (Price < 55000.01) {
            $("#ltt-ptax").val((Price * 0.005).toFixed(2));
            $("#ltt-ttax").val((Price * 0.005).toFixed(2));
        } else if (Price > 55000) {
            if (Price < 250000.01) {
                $("#ltt-ptax").val(((55000 * .005) + ((Price - 55000) * 0.01)).toFixed(2));
                $("#ltt-ttax").val(((55000 * .005) + ((Price - 55000) * 0.01)).toFixed(2));
            } else if (Price < 400000.01) {
                $("#ltt-ptax").val(((55000 * .005) + ((250000 - 55000) * 0.01) + ((Price - 250000) * .015)).toFixed(2));
                $("#ltt-ttax").val(((55000 * .005) + ((Price - 55000) * 0.01)).toFixed(2));
            } else {
                $("#ltt-ptax").val(((55000 * .005) + ((250000 - 55000) * 0.01) + ((400000 - 250000) * .015) + ((Price - 400000) * .02)).toFixed(2));
                $("#ltt-ttax").val(((55000 * .005) + ((400000 - 55000) * 0.01) + ((Price - 400000) * 0.02)).toFixed(2));
            }
        }
        sum = parseInt($("#ltt-ptax").val()) + parseInt($("#ltt-ttax").val());
        $("#ltt-total").val(sum.toFixed(2));
        return false;
    }
    $("#ltt-calc").click(lttCalc);
});

