$(document).ready(function () {
    function valign() {
        var parentHeight = $('#banner').outerHeight();
        var childHeight = $('.bannercontent').height();
        var builderlogo = $('.logo').height();
        $('.bannercontent').css('margin-top', ((parentHeight - childHeight) / 2) - builderlogo);

        
    }

    $(window).resize(function () {
        valign();
    })
    $(window).load(function () {
        valign();
         $('.bannercontent').hide().fadeIn('slow');
    })
    
    $('.arrowdown').click(function(){
         $('html, body').animate({
        scrollTop: $('#wasaga').offset().top
      },1000);
    });
    
    
   $(".nav li a[href^='#']").on('click', function(e) {

       // prevent default anchor click behavior
       e.preventDefault();

       // animate
       $('html, body').animate({
           scrollTop: $(this.hash).offset().top
         }, 1000, function(){
   
           // when done, add hash to url
           // (default click behaviour)
           window.location.hash = this.hash;
         });

    });
})