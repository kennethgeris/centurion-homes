$(document).ready(function () {

    if (!$("#map_canvas").length) {
        return false;
    }

    // JSON FILE, PIN FOLDER
    jsonMapInfo = [
        ["toronto.json", "map_pin.png"],
        ["scarborough.json", "map_pin.png"],
        ["etobicoke.json", "map_pin.png"],
        ["milton.json", "map_pin.png"],
        ["mississauga.json", "map_pin.png"],
        ["pickering.json", "map_pin.png"]

    ];

    masterMapInfo = [
        ["toronto.json", "map_pin.png"],
        ["scarborough.json", "map_pin.png"],
        ["etobicoke.json", "map_pin.png"],
        ["milton.json", "map_pin.png"],
        ["mississauga.json", "map_pin.png"],
        ["pickering.json", "map_pin.png"]

    ];

    console.log(jsonMapInfo);
    console.log(masterMapInfo);
    // POINT VARIABLES
    map = new google.maps.Map(document.getElementById("map_canvas"), {
        zoom: 12,
        center: {lat: 43.7742, lng: -79.57983},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        scrollwheel: false,
        zoomControl: true,
        draggable: !("ontouchend" in document)
    });

    function quoteIt(inVar) {
        return '"' + inVar + '"';
    }

    $("#maplocations .btn-default").click(function () {
        infowindow.close();
        for (var i = 0, n = markers.length; i < n; ++i) {
            markers[i].setMap(null);
        }

        // POINT VARIABLES       
        markers = new Array();
        mapCount = 0;
        infowindow = new google.maps.InfoWindow();
        locations = new Array();
        var marker, i;
        singlePtsAR = new Array();
        mapCount = 0;
        thsDataVal = $(this).attr("data-val");

        if (thsDataVal == 'all') {
            jsonMapInfo = masterMapInfo;
        } else {
            checkedOff = [];
            checkedOff.push([masterMapInfo[thsDataVal][0], masterMapInfo[thsDataVal][1], masterMapInfo[thsDataVal][2]]);
            jsonMapInfo = checkedOff;
        }

        jsonPts(jsonMapInfo);
    });

    //add custom buttons for the zoom-in/zoom-out on the map
    function CustomZoomControl(controlDiv, map) {
        //grap the zoom elements from the DOM and insert them in the map 
        var
                controlUIzoomIn = document.getElementById('cd-zoom-in'),
                controlUIzoomOut = document.getElementById('cd-zoom-out');

        // Setup the click event listeners and zoom-in or out according to the clicked element
        google.maps.event.addDomListener(controlUIzoomIn, 'click', function () {
            map.setZoom(map.getZoom() + 1)
        });
        google.maps.event.addDomListener(controlUIzoomOut, 'click', function () {
            map.setZoom(map.getZoom() - 1)
        });
    }

    //var zoomControlDiv = document.createElement('div');
    //var zoomControl = new CustomZoomControl(zoomControlDiv, map);

    markers = new Array();
    mapCount = 0;
    infowindow = new google.maps.InfoWindow();
    locations = new Array();
    var marker, i;

    singlePtsAR = new Array();

    jsonPts(jsonMapInfo);

    function jsonPts(inFile, inFolder, filterIt) {

        if (filterIt >= 0) {
            thsFilter = true;
            mapFile = "toronto.json";
            mapCount = filterIt;
            console.log("filter on");
        } else {
            thsFilter = false;
            mapFile = inFile[mapCount][0];
            console.log("filter off");
        }

        console.log("map count " + inFile[mapCount][0]);

        var points = $.getJSON("js/" + mapFile, function () {
        }).done(function (data) {
            $.each(data, function (key, val) {
                var singlePtAR = [];

                singlePtAR.push(val.name);
                latlongs = val.Point.coordinates;
                latlon = latlongs.split(",");
                singlePtAR.push(latlon[0]);
                singlePtAR.push(latlon[1]);
                singlePtAR.push(inFile[mapCount][1]);
                singlePtsAR.push(singlePtAR);
            });
            locations.push(singlePtsAR);

            mapCount++;
            AutoCenter();
            if (mapCount !== jsonMapInfo.length && !thsFilter) {
                jsonPts(jsonMapInfo);
            } else {
                allPoints();
            }
        }).fail(function () {
            console.log("error");
        }).always(function () {
        });
    }

    function allPoints() {

        markerCount = 0;

        for (a = 0; a < singlePtsAR.length; a++) {

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(singlePtsAR[a][2], singlePtsAR[a][1]),
                map: map,
                icon: {
                    url: "img/" + singlePtsAR[a][3],
                    scaledSize: new google.maps.Size(28, 57)
                }
            });
            markers.push(marker);

            google.maps.event.addListener(marker, 'click', (function (marker, a) {
                return function () {
                    infowindow.setContent(singlePtsAR[a][0]);
                    infowindow.open(map, marker);
                }
            })(marker, a));
        }
        AutoCenter();

    }

    function AutoCenter() {

        var infowindow = new google.maps.InfoWindow();
        var pos = new google.maps.LatLng(43.764021, -79.475549);

        marker = new google.maps.Marker({
            icon: {
                url: "img/icon-marker.png",
                scaledSize: new google.maps.Size(100, 72)
            },
            position: pos,
            map: map,
            zIndex: 9999
        });
        google.maps.event.addListener(marker, 'click', (function (marker) {
            return function () {
                infowindow.setContent('<div style="width:260px; padding:5px 5px 0 0; line-height:21px"><h5>ICON HOMES</h5><a href="https://www.google.ca/maps/place/Icon+Homes/@43.7645381,-79.4777357,17z/data=!3m1!4b1!4m5!3m4!1s0x89d4d362fc6f8281:0x7c282f16a43bd5e3!8m2!3d43.7645381!4d-79.475547"><b>Address:</b>580 Champagne Dr, Toronto, ON M3J 2T9</a><br><a href="tel:+14165468288"><b>Phone:</b> (416) 546-8288</a></div>');
                infowindow.open(map, marker);
            }
        })(marker));
        markers.push(marker);
        //  Create a new viewpoint bound
        var bounds = new google.maps.LatLngBounds();
        //  Go through each...
        $.each(markers, function (index, marker) {
            bounds.extend(marker.position);
        });
        //  Fit these bounds to the map
        map.fitBounds(bounds);
    }


    //$(window).resize(AutoCenter);
});
