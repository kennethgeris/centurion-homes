$(document).ready(function () {
                $('.carousel').carousel({
                interval: 3000 //changes the speed
            });
            $('#regClose').hide();
            $('#reg').hide();
            $('#thank').hide();

            $('.regOpen').click(function() {
                $('#lan').fadeOut("slow");
                $('#reg').fadeIn("slow");
                $('#regClose').fadeIn("slow");
                setTimeout(function(){fullHeight();},250);
            });

            $('#regClose').click(function() {
                $('#reg').fadeOut("slow");
                $('#regClose').fadeOut("slow");
                $('#lan').fadeIn("slow");
                setTimeout(function(){fullHeight();},250);
            });
            
    function fullHeight() {
        if (!isXS() || !isSM()) {
            w2 = $('.leftboard').height();
            b2 = $(window).height();
            if (w2 < b2) {
                $('.leftboard').css("height", b2);
                
                $('#myCarousel').css("height", b2);
            }else{
                $('.leftboard').css("height", w2);
                $('#myCarousel').css("height", w2);
            }
        }
    }
    
            function isXS() {
            return $("#is-xs").css("display") === "block" ? true : false;
        }
        // - - - - - - - - - - - - - - - - - - - - //
        // 
        // - - - - - - - - - - IS SM VISIBLE - - - - - - - - - - //
        function isSM() {
            return $("#is-sm").css("display") === "block" ? true : false;
        }
    $(window).load(function () {
        fullHeight();

    });
    $(window).on('resize', function () {
        fullHeight();
    });

     $('#box').drag();
});

(function($) {$.fn.drag = function(options) {

    var set = $.extend({
    handle: this,
    contain: this.parent()
    }, options),

    object = this, newX, newY,
    nadir = object.css('z-index'),
    apex = Math.pow(10,4),
    move = 'mousemove touchmove',
    release = 'mouseup touchend';

    if (window.requestAnimationFrame) var neoteric = true;

    set.handle.on('mousedown touchstart', function(e) {

    if (e.type == 'mousedown' && e.which != 1) return;

    object.css('z-index', apex);
    var marginX = set.contain.width()-object.outerWidth(),
    marginY = set.contain.height()-object.outerHeight(),
    oldX = object.position().left,
    oldY = object.position().top,
    touch = e.originalEvent.touches,
    startX = touch ? touch[0].pageX : e.pageX,
    startY = touch ? touch[0].pageY : e.pageY;

    $(window).on(move, function(e) {

    var contact = e.originalEvent.touches,
    endX = contact ? contact[0].pageX : e.pageX,
    endY = contact ? contact[0].pageY : e.pageY;
    newX = Math.max(0, Math.min(oldX+endX-startX, marginX));
    newY = Math.max(0, Math.min(oldY+endY-startY, marginY));

    if (neoteric) requestAnimationFrame(setElement);
    else setElement();
    })
    .one(release, function(e) {
    e.preventDefault();
    object.css('z-index', nadir);
    $(this).off(move).off(release);
    });

    e.preventDefault();
    });

    return this;

function setElement() {

    object.css({top: newY, left: newX});
}
};
}(jQuery));
