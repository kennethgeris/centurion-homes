$(document).ready(function () {
                $('.carousel').carousel({
                interval: 3000 //changes the speed
            });
            $('#regClose').hide();
            $('#reg').hide();
            $('#thank').hide();

            $('.regOpen').click(function() {
                $('#lan').fadeOut("slow");
                $('#reg').fadeIn("slow");
                $('#regClose').fadeIn("slow");
                setTimeout(function(){fullHeight();},250);
            });

            $('#regClose').click(function() {
                $('#reg').fadeOut("slow");
                $('#regClose').fadeOut("slow");
                $('#lan').fadeIn("slow");
                setTimeout(function(){fullHeight();},250);
            });
            
    function fullHeight() {
        if (!isXS() || !isSM()) {
            w2 = $('#lan').outerHeight();
            b2 = $(window).height();
            if (w2 < b2) {
                $('#lan, #reg').css("height", b2);
                
                $('#myCarousel').css("height", b2);
            }else{
                $('#lan, #reg').css("height", w2);
                $('#myCarousel').css("height", w2);
            }
        }
    }
    
            function isXS() {
            return $("#is-xs").css("display") === "block" ? true : false;
        }
        // - - - - - - - - - - - - - - - - - - - - //
        // 
        // - - - - - - - - - - IS SM VISIBLE - - - - - - - - - - //
        function isSM() {
            return $("#is-sm").css("display") === "block" ? true : false;
        }
    $(window).load(function () {
        fullHeight();

    });
    $(window).on('resize', function () {
        fullHeight();
    });
});
